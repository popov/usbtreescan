#!/usr/bin/env perl
#
# Copyright (c) 2014, Boris Popov <popov.b@gmail.com>
# 
# Permission to use, copy, modify, and/or distribute
# this software for any purpose with or without fee
# is hereby granted, provided that the above copyright
# notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
# ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
# DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
# THE USE OR PERFORMANCE OF THIS SOFTWARE.

use 5.014002;
use strict;
use warnings;
use Getopt::Long;
use UsbTreeScan::Scanner;
use UsbTreeScan::UsbTreeScan;

my $Version   = $UsbTreeScan::UsbTreeScan::VERSION;
my $Programm  = "usb-tree-scan";
my $Copyright = 'Copyright (c) 2014, Boris Popov <popov.b@gmail.com>';
my $UsingString = << "US";
$Programm version $Version
$Copyright

Using: $Programm [OPTION]...
--mode <mode>  or  -m <mode>,  set mode programm
                               <mode> = display | scan,
                               display - default mode;
--debug        or  -d,         print some debug info;
--version      or  -v,         print version;
--help         or  -h,         print this help.
US

sub using {
    print $UsingString;
    return;
}

sub version {
    
    print "$Version\n";
    return;
}

my $help    = undef;
my $version = undef;
my $debug;
my $mode    = 'display';

GetOptions ("help"    => \$help,
	    "debug"   => \$debug,
	    "version" => \$version,
	    "mode=s"  =>  \$mode)
    or die ("Error in command line arguments\n");

if ($help) {

    using ();
    exit 0;
}

if ($version) {

    version ();
    exit 0;
}

if ($debug) {

    $UsbTreeScan::UsbTreeScan::DEBUG = 1;
}

if ($mode eq 'display') {

    my $scanner = UsbTreeScan::Scanner->new ();
    $scanner->Scan ();
    my $tree = $scanner->GetTree ();
    unless ($tree) {

	print "\n";
	exit 0;
    }
    print "$tree";

} elsif ($mode eq 'scan') {

    my $scanner1 = UsbTreeScan::Scanner->new ();
    $scanner1->Scan ();
    say "Insert USB device and press enter...";
    getc;
    sleep 2;

    my $scanner2 = UsbTreeScan::Scanner->new ();
    $scanner2->Scan ();
    my $tree1 = $scanner1->GetTree ();
    my $tree2 = $scanner2->GetTree ();

    if ($tree1 != $tree2) {

	my $diff_tree = $tree2 - $tree1;
	print "$diff_tree";
    }

} else {

    print STDERR "Undefined mode!\n";
    print STDERR "Use --help option.\n";
    exit 1;
}

exit 0;
