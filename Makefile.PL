use 5.014002;
use ExtUtils::MakeMaker;

WriteMakefile (
    NAME              => 'UsbTreeScan',
    EXE_FILES         => ['usb-tree-scan', 'usb-tree-get-info'],
    VERSION_FROM      => 'lib/UsbTreeScan/UsbTreeScan.pm',
    PREREQ_PM         => { JSON => 0 },
    ($] >= 5.005 ?    (
	 AUTHOR         => 'Boris Popov <popov.b@gmail.com>') : ()
    ),
    );
