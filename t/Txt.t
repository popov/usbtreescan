use strict;
use warnings;
use Test::More tests => 9;

####################
=comment
ROOT
| zy avx os2
|
+--- LEVEL1
     | zy avx os2
     | zy avx os2
     |
     +--- LEVEL2
          |
          +--- LEVEL3
=end
=cut
####################

####################
BEGIN {

    use_ok ('UsbTreeScan::Txt');
};
####################

####################
my $t = UsbTreeScan::Txt->new;
ok ($t);
####################

####################
my $res;
my $awa;
$t->Renew;
$t->Set (
    name      => 'ROOT',
    level     => 0,
    content   => "zy; avx; os2",
    children  => 1
    );
$res = $t->Get ();
$awa = << "set1";
ROOT
|  avx os2
|  zy
|
set1
is ($res, $awa, "SET 1");
####################

####################
$t->Set (
    name      => 'LEVEL1',
    level     => 1,
    last_node => 1,
    content   => "zy; zy; avx; avx; os2; os2",
    children  => 1
    );
$res = $t->Get ();
$awa = << "set2";
ROOT
|  avx os2
|  zy
|
+--- LEVEL1
     |  avx
     |  avx
     |  os2
     |  os2 zy
     |  zy
     |
set2
is ($res, $awa, "SET 2");
####################

####################
$t->Set (
    name      => 'LEVEL2',
    level     => 2,
    last_node => 1,
    children  => 1
    );
$res = $t->Get ();
$awa = << "set3";
ROOT
|  avx os2
|  zy
|
+--- LEVEL1
     |  avx
     |  avx
     |  os2
     |  os2 zy
     |  zy
     |
     +--- LEVEL2
          |
set3
is ($res, $awa, "SET 3");
####################

####################
$t->Set (
    name      => 'LEVEL3',
    level     => 3,
    last_node => 1,
    children  => 1
    );
$res = $t->Get ();
$awa = << "set4";
ROOT
|  avx os2
|  zy
|
+--- LEVEL1
     |  avx
     |  avx
     |  os2
     |  os2 zy
     |  zy
     |
     +--- LEVEL2
          |
          +--- LEVEL3
               |
set4
is ($res, $awa, "SET 4");
####################

####################
$t->Renew;
$res = $t->Get ();
$awa = "";
is ($res, $awa, "RENEW");
####################

####################
$t->Set (
    name      => 'ROOT',
    level     => 0,
    content   => "zy; avx; os2",
    children  => 1
    );
$t->Set (
    name      => 'LEVEL1',
    level     => 1,
    last_node => 1,
    content   => "zy; zy; avx; avx; os2; os2",
    children  => 1
    );
$t->Set (
    name      => 'LEVEL2',
    level     => 2,
    last_node => 1,
    children  => 1
    );
$t->Set (
    name      => 'LEVEL3',
    level     => 3,
    last_node => 1,
    children  => 1
    );
$res = $t->Get ();
$awa = << "set5";
ROOT
|  avx os2
|  zy
|
+--- LEVEL1
     |  avx
     |  avx
     |  os2
     |  os2 zy
     |  zy
     |
     +--- LEVEL2
          |
          +--- LEVEL3
               |
set5
is ($res, $awa, "SET 5");
####################

####################
$t->Renew;
$t->Set (
    name      => 'ROOT',
    level     => 0,
    content   => "zy; avx; os2",
    children  => 1
    );
$t->Set (
    name      => 'LEVEL1',
    level     => 1,
    last_node => 1,
    content   => "zy; zy; avx; avx; os2; os2",
    children  => 1
    );
$t->Set (
    name      => 'LEVEL21',
    level     => 2
    );
$t->Set (
    name      => 'LEVEL22',
    level     => 2,
    last_node => 1,
    children  => 1
    );
$res = $t->Get ();
$awa = << "set6";
ROOT
|  avx os2
|  zy
|
+--- LEVEL1
     |  avx
     |  avx
     |  os2
     |  os2 zy
     |  zy
     |
     +--- LEVEL21
     |    
     +--- LEVEL22
          |
set6
is ($res, $awa, "SET 6");
####################
