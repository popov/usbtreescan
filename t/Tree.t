use strict;
use warnings;
use Test::More tests => 5;

####################
BEGIN {

    use_ok ('UsbTreeScan::Tree');
};
####################

####################
my $in = << "in";
{
    "0" : {

	"01" : {

	    "011" : { "0111" : {},
		      "0112" : {} }
	},

	"02" : { "021" : {} }
    }
}
in

my $out = << "out";
0
|
+--- 01
|    |
|    +--- 011
|         |
|         +--- 0111
|         |    
|         +--- 0112
|              
+--- 02
     |
     +--- 021
          
out

my $tree_01 = UsbTreeScan::Tree->new ( json => $in );
is ("$tree_01", $out, "JSON IN");
####################

####################
my $tree_02 = UsbTreeScan::Tree->new ( json => $in );
my $tree_03 = UsbTreeScan::Tree->new ( json => $in );
if ($tree_02 != $tree_03) {

    ok (0, "CMP 1");

} else {

    ok (1, "CMP 1");
}
####################

####################
my $in4 = << "in4";
{
    "0" : {

	"01" : {

	    "011" : { "0111" : {} }
	},

	"02" : { "021" : {} }
    }
}
in4
my $tree_04 = UsbTreeScan::Tree->new ( json => $in4 );
if ($tree_04 != $tree_03) {

    ok (1, "CMP 2");

} else {

    ok (0, "CMP 2");
}
####################

####################
my $in5 = << "in5";
{
    "0" : {

	"01" : {

	    "011" : { "0111" : {},
		      "0115" : {} }
	},

	"02" : { "021" : {} }
    }
}
in5
my $tree_05 = UsbTreeScan::Tree->new ( json => $in5 );
if ($tree_05 != $tree_03) {

    ok (1, "CMP 3");

} else {

    ok (0, "CMP 3");
}
####################

####################
my $runode = $tree_01->{root}->{children}->[0];
#my $newnode = UsbTreeScan::Tree::CopyDown ($runode);
#my $tree_down = UsbTreeScan::Tree->new ( root => $newnode );
undef $tree_01;

$out = << "down";
01
|
+--- 011
     |
     +--- 0111
     |    
     +--- 0112
          
down
#is ("$tree_down", $out, "COPY DOWN 1");
####################

####################
#my $runode2 = $tree_down->{root}->{children}->[0]->{children}->[1];
my @nodes;
#UsbTreeScan::Tree::CopyUp ($runode2, \@nodes);
#undef $tree_down;

$out = << "up";
[011]<-[]
[01]<-[]
up

my $fact_out;
foreach (@nodes) {

    $fact_out .= $_;
    $fact_out .= "\n";
}
#is ($fact_out, $out, "COPY UP 1");
####################

####################
my @ns1;
my @ns2;
my $n;

$n = UsbTreeScan::Node->new ( name => "1112" );
push @ns1, $n;

$n = UsbTreeScan::Node->new ( name => "111"  );
push @ns1, $n;

$n = UsbTreeScan::Node->new ( name => "11"   );
push @ns1, $n;

$n = UsbTreeScan::Node->new ( name => "1"    );
push @ns1, $n;

$n = UsbTreeScan::Node->new ( name => "ROOT" );
push @ns1, $n;

$n = UsbTreeScan::Node->new ( name => "1221" );
push @ns2, $n;

$n = UsbTreeScan::Node->new ( name => "122"  );
push @ns2, $n;

$n = UsbTreeScan::Node->new ( name => "12"   );
push @ns2, $n;

$n = UsbTreeScan::Node->new ( name => "1"    );
push @ns2, $n;

$n = UsbTreeScan::Node->new ( name => "ROOT" );
push @ns2, $n;

my @first_nodes;
my @nss;
push @nss, \@ns1;
push @nss, \@ns2;

#my $new_root = UsbTreeScan::Tree::GetNewRoot (\@nss, \@first_nodes);

$out = << "up";
[1112]<-[]
[1221]<-[]
up

$fact_out = "";
foreach (@first_nodes) {

    $fact_out .= $_;
    $fact_out .= "\n";
}
#is ($fact_out, $out, "GetNewRoot  1");
####################

####################
#my $ttt = UsbTreeScan::Tree->new ( root => $new_root );
$out = << "up";
ROOT
|
+--- 1
     |
     +--- 11
     |    |
     |    +--- 111
     |         |
     |         +--- 1112
     |              
     +--- 12
          |
          +--- 122
               |
               +--- 1221
                    
up
#is ("$ttt", $out, "GetNewRoot  2");
####################

####################
my @ns4;
$n = UsbTreeScan::Node->new ( name => "11a"   );
push @ns4, $n;
$n = UsbTreeScan::Node->new ( name => "1a"    );
push @ns4, $n;
$n = UsbTreeScan::Node->new ( name => "ROOTa" );
push @ns4, $n;
my @first_nodes2;
my @nss4;
push @nss4, \@ns4;
#my $new_root2 = UsbTreeScan::Tree::GetNewRoot (\@nss4, \@first_nodes2);
#my $ttt2 = UsbTreeScan::Tree->new ( root => $new_root2 );
$out = << "up";
ROOTa
|
+--- 1a
     |
     +--- 11a
          
up
#is ("$ttt2", $out, "GetNewRoot 3");
####################
