use strict;
use warnings;
use Test::More tests => 10;

BEGIN {

    use_ok ('UsbTreeScan::ContentBuilder');
};

my $cb = UsbTreeScan::ContentBuilder->new;
ok ($cb);

$cb->Build ();
is ( scalar( @{$cb->Get ()} ), 0, "SIZE BUILD 1");

my $content = "Compiler = gcc; x = b; ert => 123; Q: no problem;";
$cb->Build (
    content    => $content,
    max_length => 5
    );
is ( scalar( @{$cb->Get ()} ), 4, "SIZE BUILD 2");

$cb->Build (
    content    => $content,
    max_length => 155
    );
is ( scalar( @{$cb->Get ()} ), 1, "SIZE BUILD 2");

my $ins = "\n  12345\n\n  ";
is ( UsbTreeScan::ContentBuilder::Trim ($ins),
     12345,
     "ContentBuilder::Trim 1" );

$ins = "\n  \n\n  ";
is ( UsbTreeScan::ContentBuilder::Trim ($ins),
     "",
     "ContentBuilder::Trim 2" );

$content = "123; 456; 789; 000; abc; def; zxy";
$cb->Build (
    content    => $content,
    max_length => 7
    );
is ( scalar( @{$cb->Get ()} ), 4, "SIZE BUILD 3");

$content = "kaka;";
$cb->Build (
    content    => $content,
    max_length => 7
    );
is ( scalar( @{$cb->Get ()} ), 1, "SIZE BUILD 4");

$content = "kaka";
$cb->Build (
    content    => $content,
    max_length => 7
    );
is ( scalar( @{$cb->Get ()} ), 1, "SIZE BUILD 5");

=comment
foreach  (@{$cb->Get ()}) {

    diag "\n>$_<\n";
}
=end
=cut
