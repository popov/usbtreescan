use strict;
use warnings;
use Test::More;
use File::Spec::Functions;
use Cwd;

BEGIN {

    use_ok ('UsbTreeScan::Scanner');
};

my $scanner1 = UsbTreeScan::Scanner->new ();
is ($scanner1->GetTree, undef, 'TREE FAULT');
is ($scanner1->{path}, '/sys/bus/usb/devices/', 'SCANNER1 PATH');

my $scanner2 = UsbTreeScan::Scanner->new ( path => '.');
is ($scanner2->{path}, '.', 'SCANNER2 PATH');

$scanner1->Scan;
my $root = undef;
$root = $scanner1->{root};
ok ($root);

my $tree1 = $scanner1->GetTree;
ok ($tree1);
ok ("$tree1");

my $scanner3 = UsbTreeScan::Scanner->new ( path => '/' );
$scanner3->Scan;

my $tree3 = $scanner3->GetTree;
unless ($tree3) {

    ok (1);

} else {

   ok (0);
}

my $dir = getcwd ();
my $tdir = catfile ($dir, 't');
my $scanner4 = UsbTreeScan::Scanner->new ( path => $tdir );
$scanner4->Scan;
my $tree4 = $scanner4->GetTree;

=comment
if ($tree4) {

    ok (1);

} else {

    ok (0);
}
=end
=cut

done_testing ();
