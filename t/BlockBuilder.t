use strict;
use warnings;
use Test::More tests => 25;

####################
BEGIN {

    use_ok ('UsbTreeScan::BlockBuilder');
};
####################

####################
my $bb = UsbTreeScan::BlockBuilder->new;
ok ($bb);
####################

####################
my $ee = $bb->get_regular_prefix (2, [0,1,2]);
is ($ee, "|    ", "get_regular_prefix 1");
####################

####################
$ee = $bb->get_regular_prefix (0, [ 0 ]);
is ($ee, "|    ", "get_regular_prefix 2");
####################

####################
my $b1 = << "b1";
ROOT
|
b1
is ($bb->Build (
	name      => 'ROOT',
	level     => 0,
	levels    => [ 0 ],
	children  => 1
    ), $b1, 'Build 2');
####################

####################
my $b2 = << "b2";
ROOT
|  kaka
|
b2
is ($bb->Build (
	name      => 'ROOT',
	level     => 0,
	content   => 'kaka',
	levels    => [ 0 ],
	children  => 1
    ), $b2, 'Build 2');
####################

####################
my $b3 = << "b3";
+--- LEVEL1
|    |  arc trtw
|    |
b3
is ($bb->Build (
	name      => 'LEVEL1',
	level     => 1,
	content   => 'arc trtw',
	levels    => [0, 1],
	children  => 1
    ), $b3, 'Build 3');
####################

####################
my $b4 = << "b4";
ROOT
  kaka
b4
is ($bb->Build (
	name      => 'ROOT',
	level     => 0,
	content   => 'kaka',
	levels    => []
    ), $b4, 'Build 4');
####################

####################
my $b5 = << "b5";
ROOT
|  kaka
|
b5
is ($bb->Build (
	name      => 'ROOT',
	level     => 0,
	content   => 'kaka',
	levels    => [ 0 ],
	children  => 1
    ), $b5, 'Build 5');
####################

####################
my $b6 = << "b6";
ROOT
|  kaka
|
b6
is ($bb->Build (
	name      => 'ROOT',
	level     => 0,
	content   => 'kaka',
	levels    => [ 0, 1 ],
	children  => 1
    ), $b6, 'Build 6');
####################

####################
my $b7 = << "b7";
ROOT
|  give
|  idiot me
|  money
|  now
|
b7
is ($bb->Build (
	name      => 'ROOT',
	level     => 0,
	content   => 'give; me; money; now; idiot',
	levels    => [ 0, 1 ],
	children  => 1
    ), $b7, 'Build 7');
####################

####################
my $b8 = << "b8";
ROOT
  give
  idiot me
  money
  now
b8
is ($bb->Build (
	name      => 'ROOT',
	level     => 0,
	content   => 'give; me; money; now; idiot',
	levels    => [ ]
    ), $b8, 'Build 8');
####################

####################
my $b9 = << "b9";
|    +--- LEVEL2
|    |    |  arc trtw
|    |    |
b9
is ($bb->Build (
	name      => 'LEVEL2',
	level     => 2,
	content   => 'arc trtw',
	levels    => [0, 1, 2],
	children  => 1
    ), $b9, 'Build 9');
####################

####################
my $b10 = << "b10";
|    +--- LEVEL2
|    |      arc trtw
|    |    
b10
is ($bb->Build (
	name      => 'LEVEL2',
	level     => 2,
	content   => 'arc trtw',
	levels    => [0, 1]
    ), $b10, 'Build 10');
####################

####################
my $b11 = << "b11";
|    +--- LEVEL2
|           arc trtw
|         
b11
is ($bb->Build (
	name      => 'LEVEL2',
	level     => 2,
	content   => 'arc trtw',
	levels    => [ 0 ]
    ), $b11, 'Build 11');
####################

####################
my $b12 = << "b12";
     +--- LEVEL2
            arc trtw
          
b12
is ($bb->Build (
	name      => 'LEVEL2',
	level     => 2,
	content   => 'arc trtw',
	levels    => [ ]
    ), $b12, 'Build 12');
####################

####################
my $b13 = << "b13";
     +--- LEVEL2
            give
            idiot
            me
            money
            now
          
b13
is ($bb->Build (
	name      => 'LEVEL2',
	level     => 2,
	content   => 'give; me; money; now; idiot',
	levels    => [ ]
    ), $b13, 'Build 13');
####################

####################
my $b14 = << "b14";
|    +--- LEVEL2
|           give
|           idiot
|           me
|           money
|           now
|         
b14
is ($bb->Build (
	name      => 'LEVEL2',
	level     => 2,
	content   => 'give; me; money; now; idiot',
	levels    => [ 0 ]
    ), $b14, 'Build 14');
####################

####################
my $b15 = << "b15";
|    +--- LEVEL2
|    |      give
|    |      idiot
|    |      me
|    |      money
|    |      now
|    |    
b15
is ($bb->Build (
	name      => 'LEVEL2',
	level     => 2,
	content   => 'give; me; money; now; idiot',
	levels    => [ 0, 1 ]
    ), $b15, 'Build 15');
####################

####################
my $b16 = << "b16";
|    +--- LEVEL2
|    |    |  give
|    |    |  idiot
|    |    |  me
|    |    |  money
|    |    |  now
|    |    |
b16
is ($bb->Build (
	name      => 'LEVEL2',
	level     => 2,
	content   => 'give; me; money; now; idiot',
	levels    => [ 0, 1, 2 ],
	children  => 1
    ), $b16, 'Build 16');
####################

####################
my $b17 = << "b17";
|    |    +--- LEVEL3
|    |    |    |  give
|    |    |    |  idiot
|    |    |    |  me
|    |    |    |  money
|    |    |    |  now
|    |    |    |
b17
is ($bb->Build (
	name      => 'LEVEL3',
	level     => 3,
	content   => 'give; me; money; now; idiot',
	levels    => [ 0, 1, 2, 3 ],
	children  => 1
    ), $b17, 'Build 17');
####################

####################
my $b18 = << "b18";
|    |    +--- LEVEL3
|    |    |      give
|    |    |      idiot
|    |    |      me
|    |    |      money
|    |    |      now
|    |    |    
b18
is ($bb->Build (
	name      => 'LEVEL3',
	level     => 3,
	content   => 'give; me; money; now; idiot',
	levels    => [ 0, 1, 2 ]
    ), $b18, 'Build 18');
####################

####################
my $b19 = << "b19";
|         +--- LEVEL3
|         |      give
|         |      idiot
|         |      me
|         |      money
|         |      now
|         |    
b19
is ($bb->Build (
	name      => 'LEVEL3',
	level     => 3,
	content   => 'give; me; money; now; idiot',
	levels    => [ 0, 2 ]
    ), $b19, 'Build 19');
####################

####################
my $b20 = << "b20";
          +--- LEVEL3
          |      give
          |      idiot
          |      me
          |      money
          |      now
          |    
b20
is ($bb->Build (
	name      => 'LEVEL3',
	level     => 3,
	content   => 'give; me; money; now; idiot',
	levels    => [ 2 ]
    ), $b20, 'Build 20');
####################

####################
my $b21 = << "b21";
          +--- LEVEL3
                 give
                 idiot
                 me
                 money
                 now
               
b21
is ($bb->Build (
	name      => 'LEVEL3',
	level     => 3,
	content   => 'give; me; money; now; idiot',
	levels    => [ ]
    ), $b21, 'Build 21');
####################
