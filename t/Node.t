use strict;
use warnings;
use Test::More;

BEGIN {

    use_ok ('UsbTreeScan::Node');
};

my $node_00 = UsbTreeScan::Node->new ();
my $node_01 = UsbTreeScan::Node->new ();
my $node_02 = UsbTreeScan::Node->new ();

is ($node_00->{name}, "");

$node_00->{name} = "ROOT";
$node_01->{name} = "1";
$node_02->{name} = "2";

is ($node_00->{name}, "ROOT");
is ($node_01->{name}, 1);
is ($node_02->{name}, 2);

my $node_03 = UsbTreeScan::Node->new ( name => "3" );
is ($node_03->{name}, "3");

#diag ( "\n------> $node_00->{children} <------\n" );
#diag ( "\n------> @{$node_00->{children}} <------\n" );
is (scalar @{$node_00->{children}}, 0);

if (! defined $node_00->{parent}) {

    ok (1);

} else {

    ok (0);
}

my $one;
my $two;

$node_00 = $node_00 << $node_01;
$node_00 = $node_00 << $node_02;

#is ($node_01->{parent}->{name}, "ROOT");
#is ($node_02->{parent}->{name}, "ROOT");

#diag ( "\n------> @{$node_00->{children}}[0]->{name} <------\n" );

is (@{$node_00->{children}}[0]->{name}, 1);
is (@{$node_00->{children}}[1]->{name}, 2);

is ("$node_00", "[ROOT]<-[[1][2]]");
is ("$node_01", "[1]<-[]");
is ("$node_02", "[2]<-[]");

if ($node_00 == $node_01) {

    ok (0);

} else {

    ok (1);
}

if ($node_00 != $node_01) {

    ok (1);

} else {

    ok (0);
}

my $node_00_new = UsbTreeScan::Node->new ( name => "ROOT" );
my $node_01_new = UsbTreeScan::Node->new ( name => "1" );
$node_00_new = $node_00_new << $node_01_new;
if ($node_01_new == $node_01) {

    ok (1);

} else {

    ok (0);
}

unless ( $node_00->{content} ) {

    ok (1);

} else {

    ok (0);
}

my $content = {

    name => "nik",
    city => "moscow"
};

$node_00->{content} = $content;

if ( $node_00->{content} ) {

    ok (1);

} else {

    ok (0);
}

is ($node_00->{content}->{name}, "nik");

my $node_00_02 = UsbTreeScan::Node->new ( content => $content );
is ($node_00_02->{content}->{city}, "moscow");

#TODO

done_testing ();
# its man page ( perldoc Test::More ) for help writing this test script.
