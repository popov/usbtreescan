use strict;
use warnings;
use Test::More;

BEGIN {

    use_ok ('UsbTreeScan::ContentSeeker');
};

my $cs = UsbTreeScan::ContentSeeker->new ();
is ( $cs->in_black (''), 0, 'NO BLACK' );
is ( $cs->in_black ('report_descriptor'), 1, 'BLACK' );
is ( $cs->in_white (''), 0, 'NO WHITE' );
is ( $cs->in_white ('manufacturer'), 1, 'WHITE' );

#
#TODO
#

done_testing ();
