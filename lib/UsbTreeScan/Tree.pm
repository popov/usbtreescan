# Copyright (c) 2014, Boris Popov <popov.b@gmail.com>
# 
# Permission to use, copy, modify, and/or distribute
# this software for any purpose with or without fee
# is hereby granted, provided that the above copyright
# notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
# ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
# DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
# THE USE OR PERFORMANCE OF THIS SOFTWARE.

package UsbTreeScan::Tree;

use 5.014002;
use strict;
use warnings;
use JSON;
use UsbTreeScan::Txt;
use UsbTreeScan::Node;
use overload
    '!='  => "NEQ",
    '-'   => "MINUS",
    '""'  => "String";

sub new {

    my $class = shift;
    my $self = { @_ };
    bless ($self, $class);
    $self->{Txt} = UsbTreeScan::Txt->new;

    if (defined $self->{json}) {

	$self->parse_json ( $self->{json} );
    }
    return $self;
}

sub GetProductInfo {

    my $self = shift;
    my $major = shift;
    my $minor = shift;
    my $major_minor = "$major:$minor";
    my %result;
    my $root = $self->{root};

    $self->{searched} = 0;
    undef $self->{node_dev};

    $self->get_node_dev ($major_minor, $root);
    if ($self->{node_dev}) {

	$self->{searched} = 0;
	undef $self->{node_product};

	$self->get_node_product ($self->{node_dev});
	if ($self->{node_product}) {

	    $result{serial} = $self->{node_product}->{content}->{serial};
	    $result{product} = $self->{node_product}->{content}->{product};
	    $result{manufacturer} = $self->{node_product}->{content}->{manufacturer};
    	}
    }
    return \%result;
}

sub get_node_product {

    my ($self, $node) = @_;
    return unless defined $node;

    if ( $self->has_product_info ($node) ) {

	$self->{node_product} = $node;
	return;
    }

    my $n = $node->{parent};
    while ($n) {

	if ( $self->has_product_info ($n) ) {

	    $self->{node_product} = $n;
	    last;
	}
	$n = $n->{parent};
    }
    return;
}

sub has_product_info {

    my ($self, $node) = @_;
    return 0 unless defined $node->{content};
    return 1 if defined $node->{content}->{serial};
    return 1 if defined $node->{content}->{product};
    return 1 if defined $node->{content}->{manufacturer};
    return 0;
}

sub get_node_dev  {

    my $self = shift;
    my $major_minor = shift;

    foreach my $n (@_) {

	if ($self->{searched}) {
	    
	    last;
	}

	if ( $self->has_it_us_dev ($n, $major_minor) ) {

	    $self->{searched} = 1;
	    $self->{node_dev} = $n;
	    last;

	} else {

	    if ($n->{children}) {

		$self->get_node_dev ($major_minor, @{$n->{children}});
	    }
	}
    }
    return;
}

sub has_it_us_dev {

    my ($self, $node, $major_minor) = @_;
    return 0 unless defined ($node);
    return 0 unless defined ($node->{content});
    return 0 unless defined ($node->{content}->{dev});

    if ($node->{content}->{dev} eq $major_minor) {

	return 1;
    } 
    return 0;
}

sub parse_json {

    my ($self, $json) = @_;
    return unless $json;
    my $jp = JSON->new->allow_nonref;

    my $hash = $jp->decode ($json);

    my @ks = keys %$hash;
    return unless scalar (@ks);

    my $key = $ks[0];
    $self->walk_json ($key, $hash, undef);
    return;
}

sub walk_json {

    my ($self, $key, $hash, $parent) = @_;
    my $node = UsbTreeScan::Node->new ( name => $key );

    unless ($self->{root}) {

	$self->{root} = $node;
    }

    if ($parent) {

	$parent = $parent << $node;
    }

    my $new_hash = $hash->{$key};
    my @ks = sort keys %$new_hash;
    my $num = scalar (@ks);
    return unless ($num);

    for (my $i = 0; $i < $num; $i++) {

	my $n_key = $ks[$i];
	$self->walk_json ($n_key, $new_hash, $node);
    }
    return;
}

sub String {

    my $self = shift;
    $self->{Txt}->Renew;
    return '' unless (defined $self->{root});
    $self->MainWalk ("text");
    return $self->{Txt}->Get;
}

sub MainWalk {

    my ($self, $mode, $out) = @_;

    my $element = { level => 0, node => $self->{root} };
    if ( scalar @{$self->{root}->{children}} ) {

	$element->{children} = 1;
    }

    $self->walk ($element, $mode, $out);
    return;
}

sub doit {

    my ($self, $element_ref, $mode, $out) = @_;

    my $level     = $element_ref->{level};
    my $node      = $element_ref->{node};
    my $last_node = $element_ref->{last_node};
    my $children  = $element_ref->{children};
    my $content   = $node->{content};

    my $cont = undef;
    if ($content) {

	my @k = keys %$content;
	my $str = "";
	foreach (@k) {

	    if ($_ ne "uevent") {

		$str .= $_;
		$str .= '->';
		$str .= $content->{$_};
		$str .= ';';
		$cont = $str;
	    }
	}
    }

    if ($mode eq "text") {

	$self->{Txt}->Set (
	    level     => $level,
	    last_node => $last_node,
	    name      => $node->{name},
	    children  => $children,
	    content   => $cont
	    );

    } elsif ($mode eq "nodelist") {

	push @$out, $node;

    } elsif ($mode eq "levels") {

	unless ( defined $out->{$level} ) {

	    $out->{$level} = [];
	}
	push @{$out->{$level}}, $node;
    }

    return;
}

sub walk {

    my ($self, $element, $mode, $out) = @_;
    return unless ($element);

    $self->doit ($element, $mode, $out);

    my $newlevel = $element->{level} + 1;
    my $size = scalar ( @{$element->{node}->{children}} );
    for (my $i = 0; $i < $size; $i++) {

	my $node = ${$element->{node}->{children}}[$i];
	my $el = { level => $newlevel, node  => $node };
	if ( $i == ($size - 1) ) {

	    $el->{last_node} = 1;
	}

	if ( scalar @{$node->{children}} ) {

	    $el->{children} = 1;
	}
	$self->walk ($el, $mode, $out);
    }
    return;
}

sub NEQ {

    my ($left, $right) = @_;
    my $left_list = [];
    my $right_list = [];
    
    $left->MainWalk ("nodelist", $left_list);
    $right->MainWalk ("nodelist", $right_list);

    my $nums = scalar (@$left_list);
    if ( $nums != scalar (@$right_list) ) {

	return 1;
    }

    for (my $i = 0; $i < $nums; $i++) {

	return 1 if ($left_list->[$i] != $right_list->[$i]);
    }

    return 0;
}

sub compare_list {

    my ($new_list, $old_list, $out_list) = @_;
    foreach (@$new_list) {

	my $node = $_;
	my $name = $_->{name};

	my $yes = 0;
	foreach (@$old_list) {

	    if ($_->{name} eq $name) {

		$yes = 1;
	    }
	}

	unless ($yes) {


	    push @$out_list, $node;
	}
    }
    return;
}

sub MINUS {

    my ($new, $old) = @_;
    my $root = UsbTreeScan::Node->new (name => "diff_tree");

    my $out_list = [];
    my $new_list = [];
    my $old_list = [];

    push @$new_list, $new->{root};
    push @$old_list, $old->{root};

    compare_list ($new_list, $old_list, $out_list);
    while ( scalar (@{$out_list}) == 0 ) {

	my @tempn;
	foreach (@$new_list) {

	    push @tempn, @{$_->{children}};
	}

	last unless (scalar @tempn);
	$new_list = \@tempn;
	
	my @tempo;
	foreach (@$old_list) {

	    push @tempo, @{$_->{children}};
	}

	last unless (scalar @tempo);
	$old_list = \@tempo;

	compare_list ($new_list, $old_list, $out_list);
    }
    
    if (scalar @$out_list) {

	foreach (@$out_list) {

	    $root = $root << $_;
	}
    }
    return UsbTreeScan::Tree->new (root => $root);
}

1;
