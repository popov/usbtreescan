# Copyright (c) 2014, Boris Popov <popov.b@gmail.com>
# 
# Permission to use, copy, modify, and/or distribute
# this software for any purpose with or without fee
# is hereby granted, provided that the above copyright
# notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
# ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
# DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
# THE USE OR PERFORMANCE OF THIS SOFTWARE.

package UsbTreeScan::Node;

use 5.014002;
use strict;
use warnings;
use overload
    '<<'  => "Unite",
    '=='  => "EQ",
    '!='  => "NEQ",
    '""'  => "AsString";

sub new {

    my $class = shift;
    my $self = {@_};


    if (! defined $self->{name} ) {

	$self->{name} = "";
    }
    
    bless ($self, $class);
    $self->{children} = [];
    $self->{parent} = undef;
    return $self;
}

sub Unite {

    my $left = shift;
    my $right = shift;
    push @{$left->{children}}, $right;
    $right->{parent} = $left;
    return $left;
}

sub get_children_names {

    my $self = shift;
    my @l = @{$self->{children}};

    my $result = "";
    foreach my $child (@l) {

	$result .= "[";
	$result .= $child->{name};
	$result .= "]";
    }

    return $result;
}

sub AsString {

    my $self = shift;
    my $result = "[".$self->{name}."]";
    $result .= "<-";
    $result .= "[".get_children_names ($self)."]";
    return $result;
}

sub EQ {

    my $left = shift;
    my $right = shift;

    return 0 if ( $left->{name} ne $right->{name} );
    my @lch = @{$left->{children}};
    my @rch = @{$right->{children}};
    return 0 if (scalar (@lch) != scalar (@rch));

    for (my $i = 0; $i < scalar (@lch); $i++) {

	my $lname = $lch[$i]->{name};
	my $rname = $rch[$i]->{name};
	return 0 unless ($lname eq $rname);
    }

    return 1;
}

sub NEQ {

    return (! EQ (shift, shift) );
}

1;
