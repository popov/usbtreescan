# Copyright (c) 2014, Boris Popov <popov.b@gmail.com>
# 
# Permission to use, copy, modify, and/or distribute
# this software for any purpose with or without fee
# is hereby granted, provided that the above copyright
# notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
# ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
# DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
# THE USE OR PERFORMANCE OF THIS SOFTWARE.

package UsbTreeScan::BlockBuilder;

use 5.014002;
use strict;
use warnings;
use UsbTreeScan::ContentBuilder;

sub new {

    my $class = shift;
    my $self = { @_ };
    bless ($self, $class);
    $self->{shift} = 4;
    $self->{CB} = UsbTreeScan::ContentBuilder->new;
    return $self;
}

sub Build {

    my $self = shift;
    my $parameters  = { @_ };

    my $name     = $parameters->{name};
    my $level    = $parameters->{level};
    my $content  = $parameters->{content};
    my $levels   = $parameters->{levels};
    my $children = $parameters->{children};

    return "" unless (defined $name);
    return "" unless (defined $level);
    return "" unless (defined $levels);

    my @strings = $self->GetStrings ($name, $level,
				     $content, $levels,
				     $children);

    my $result = "";
    foreach (@strings) {

	$result .= $_;
    }

    return $result;
}

=comment

ROOT
| z=y avx os/2
|
+--- LEVEL1
|    | z=y avx os/2
|    | z=y avx os/2
|    |
|    +--- LEVEL2
|    |    |
|    |    +--- LEVEL3
|    |         |
|    |         +---...

=end
=cut

sub GetStrings {

    my $self     = shift;
    my $name     = shift;
    my $level    = shift;
    my $content  = shift;
    my $levels   = shift;
    my $children = shift;

    my @strings;
    my @cont = $self->get_content ($name, $content);

    #TODO!!
    my $num = scalar (@cont) + 2; #?2

    for (my $i = 0; $i < $num; $i++) {

	unless ($i) {

	    push @strings, $self->get_first
		($name, $level, $levels);

	} else {

	    my $s = $self->get_regular ($level,
					$levels,
					\@cont,
					$i,
					$children);
	    if ($s) {

		push @strings, $s;
	    }
	}
    }
    return @strings;
}

sub get_first {

    my ($self, $name, $level, $levels) = @_;
    my $result = '';

    for (my $i = 0; $i < $level; $i++) {

	if ($i == ($level - 1)) {

	    $result .= $self->get_node_prefix ();

	} else {
	    
	    $result .= $self->get_regular_prefix ($i, $levels);
	}
    }

    $result .= $name."\n";
    return $result;
}

sub in {

    my ($self, $level, $levels) = @_;
    foreach (@$levels) {

	if ($_ == $level) {

	    return 1;
	}
    }
    return 0;
}

sub get_regular_prefix {

    my ($self, $level, $levels) = @_;
    my $result = "";

    if ($self->in ($level, $levels)) {

	$result .= "|";

    } else {

	$result .= " ";
    }

    $result .= $self->get_n_space ($self->{shift});
    return $result;
}

sub get_n_space {

    my $self = shift;
    my $n = shift;
    my $result = '';

    for (my $i = 0; $i < $n; $i++) {

	$result .= " ";
    }
    return $result;
}

sub get_node_prefix {

    my $self = shift;
    my $result = '';

    for (my $i = 0; $i <= $self->{shift}; $i++) {

	if ($i == 0) {

	    $result .= '+';

	} elsif ($i == $self->{shift}) {

	    $result .= ' ';

	} else {

	    $result .= '-';
	}
    }
    return $result;
}

sub get_regular {

    my ($self, $level, $levels,
	$cont, $num, $children) = @_;
    my $result = '';
    my $content = $cont->[$num - 1];

    for (my $i = 0; $i < $level; $i++) {

	$result .= $self->get_regular_prefix ($i, $levels);
    }

    if ($self->in ($level, $levels)) {

	if ($children) {

	    $result .= '|';
	}
    }

    if (defined $content) {

	$result .= $self->get_n_space ( $self->{shift} / 2 );
	$result .= $content;
    }

    if ($result) {

	$result .= "\n";
    }

    return $result;
}

sub get_content {

    my $self = shift;
    my $name = shift;
    my $content = shift;

    my $length = length $name;
    if ( (length $name) < 5 ) {

	$length *= 2;
    }

    $self->{CB}->Build (

	content    => $content,
	max_length => $length
	);

    my $cont = $self->{CB}->Get ();
    my @result = @{$cont};
    return @result;
}

1;
__END__
# Below is stub documentation for your module. You'd better edit it!
