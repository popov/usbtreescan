# Copyright (c) 2014, Boris Popov <popov.b@gmail.com>
# 
# Permission to use, copy, modify, and/or distribute
# this software for any purpose with or without fee
# is hereby granted, provided that the above copyright
# notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
# ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
# DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
# THE USE OR PERFORMANCE OF THIS SOFTWARE.

package UsbTreeScan::Scanner;

use 5.014002;
use strict;
use warnings;
use File::Spec::Functions;
use UsbTreeScan::Node;
use UsbTreeScan::ContentSeeker;
use UsbTreeScan::Tree;

my $ROOT_PATH  = '/sys/bus/usb/devices/';
my $ROOT_TEMPL = '^usb*';
my @DIRNAMES = qw[

^\d+.*
^tty
^ep
^host
^target
^block
^sd
usb

];

sub new {

    my $class = shift;
    my $self = { @_ };

    unless ( $self->{path} ) {

	$self->{path} = $ROOT_PATH;
    }

    $self->{seeker} = UsbTreeScan::ContentSeeker->new ();
    bless ($self, $class);
    return $self;
}

sub delete {

   my $self = shift;
   foreach my $node (@_) {

       undef $node->{parent};
       my @ch = @{$node->{children}};
       undef $node->{children};
       $self->delete (@ch);
   }
   return;
}

sub remove_cycl_link {

    my $self = shift;
    if ( defined $self->{root} ) {

	$self->delete ( @{$self->{root}} );
	undef $self->{root};
    }

    return;
}

sub Scan {

    my $self = shift;
    $self->remove_cycl_link;
    $self->remove_tree;

    opendir (my $dh, $self->{path}) ||
	die "Can't open dir $self->{path}: $!";

    my @files = readdir $dh;
    my @curdir;

    foreach (sort @files) {

	if ( m/$ROOT_TEMPL/ ) {

	    push @curdir, $_;
	}
    }
    closedir $dh;

    return unless ( scalar (@curdir) );

    foreach (@curdir) {

	my $name = catfile ($self->{path}, $_);
	next unless ( -l $name );
	my $node = UsbTreeScan::Node->new ( name => $name );
	push @{$self->{root}}, $node;
    }

    foreach ( @{$self->{root}} ) {

	$self->ScanNode ($_);
    }

    return;
}

sub ScanNode {

    my $self = shift;
    my $current = shift;

    opendir (my $dh, $current->{name}) ||
	die "Can't open dir $current->{name}: $!";

    my @files = readdir $dh;
    foreach my $file (sort @files) {

	next if ($file eq '.');
	next if ($file eq '..');

	my $short = $file;
	my $name = catfile ($current->{name}, $file);

	if (-d $name) {

	    foreach (@DIRNAMES) {

		my $pattern = $_;
		if ( $short =~ m/$pattern/ ) {

		    my $newnode = UsbTreeScan::Node->new ( name => $name );
		    $current = $current << $newnode;
		}
	    }

	} elsif (-f $name) {

	    $self->{seeker}->Seek ($short, $name, $current);
	}
    }
    closedir $dh;

    foreach my $node ( @{$current->{children}} ) {

	$self->ScanNode ($node);
    }

    return;
}

sub GetTree {

    my $self = shift;
    return unless defined $self->{root};
    return undef unless ( scalar @{$self->{root}} );

    my $rootnode = UsbTreeScan::Node->new ( name => $self->{path} );
    foreach (@{$self->{root}}) {

	$rootnode = $rootnode << $_;
    }

    my $tree = UsbTreeScan::Tree->new ( root => $rootnode );
    $self->{tree} = $tree;
    return $tree;
}

sub remove_tree {

   my $self = shift;
   return unless defined $self->{tree};
   undef $self->{tree}->{children};
   undef $self->{tree};
   return;
}

sub DESTROY {

    my $self = shift;
    $self->remove_cycl_link;
    $self->remove_tree;
    return;
}

1;
