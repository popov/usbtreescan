# Copyright (c) 2014, Boris Popov <popov.b@gmail.com>
# 
# Permission to use, copy, modify, and/or distribute
# this software for any purpose with or without fee
# is hereby granted, provided that the above copyright
# notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
# ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
# DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
# THE USE OR PERFORMANCE OF THIS SOFTWARE.

package UsbTreeScan::ContentSeeker;

use 5.014002;
use strict;
use warnings;

use UsbTreeScan::UsbTreeScan;

my @BLACK_LIST = qw [

descriptors
report_descriptor
supports_autosuspend
configuration
modalias
quirks
avoid_reset_quirk
authorized
authorized_default
bAlternateSetting
bNumConfigurations
bConfigurationValue
events
events_async
events_poll_msecs
stat
inflight
evt_media_change
queue_depth
queue_type
capability
discard_alignment
alignment_offset
ext_range
range

];

my @NAME_LIST = qw [

manufacturer
product
idVendor
idProduct
dev
speed
serial
busnum
devnum
devpath
version
interface
bInterfaceClass
bInterfaceSubClass
bInterfaceProtocol
bcdDevice
bMaxPower
bNumEndpoints
bInterfaceNumber
bNumInterfaces
bDeviceProtocol
bDeviceClass
bDeviceSubClass
bMaxPacketSize0
bmAttributes
urbnum
maxchild
bLength
bEndpointAddress
bInterval
wMaxPacketSize
interval
type
direction
port_number
vendor
model
size
ro
timeout
partition
ioerr_cnt
iodone_cnt
iorequest_cnt
iocounterbits
scsi_level
rev
removable
start
state
max_sectors
device_blocked
uevent

];

sub new {

    my $class = shift;
    my $self = { @_ };
    bless ($self, $class);
    $self->{nameslist} = \@NAME_LIST;
    $self->{blacklist} = \@BLACK_LIST;
    return $self;
}

sub in_ {

    my $self = shift;
    my $in = shift;
    my $list = shift;

    foreach ( @{$list} ) {

	return 1 if ($_ eq $in);
    }

    return 0;
}

sub in_black {

    my $self = shift;
    my $in = shift;

    return $self->in_ ( $in, $self->{blacklist} );
}

sub in_white {

    my $self = shift;
    my $in = shift;

    return $self->in_ ( $in, $self->{nameslist} );
}

sub Seek {

    my $self = shift;
    my $short_name = shift;
    my $full_name = shift;
    my $node = shift;

    if ( $self->in_black ($short_name) ) {

	return;
    }

    unless ( -r $full_name ) {

	return;
    }

    unless ( $self->in_white ($short_name) ) {

	my @result = get_content ($full_name);
	return unless ($result[0]);

	if ($UsbTreeScan::UsbTreeScan::DEBUG) {

	    print STDERR "$short_name->\{$result[1]\}\n";
	}

	return;
    }

    my @result = get_content ($full_name);
    return unless ($result[0]);

    unless ( $node->{content} ) {

	$node->{content} = { };
    }

    $node->{content}->{$short_name} = $result[1];
    return;
}

sub get_content {

    my $name = shift;
    my @result = (0, 0);

    open (my $fh, "<", $name) or die "Cannot open < $name: $!";
    my $content = "";
    while ( read ($fh, my $temp, 4096) ) {

	$content = $content.$temp;
    }
    close ($fh);

    chomp ($content);
    $result[0] = 1;
    $result[1] = $content;
    return @result;
}

1;
