# Copyright (c) 2014, Boris Popov <popov.b@gmail.com>
# 
# Permission to use, copy, modify, and/or distribute
# this software for any purpose with or without fee
# is hereby granted, provided that the above copyright
# notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
# ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
# DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
# THE USE OR PERFORMANCE OF THIS SOFTWARE.

package UsbTreeScan::ContentBuilder;

use 5.014002;
use strict;
use warnings;

sub Trim {

    my $is = shift;
    $is =~ s/^\s+//;
    $is =~ s/\s+$//;
    return $is;
}

sub new {

    my $class = shift;
    my $self = { @_ };
    bless ($self, $class);
    $self->{result} = [ ];

    $self->{split} = ";";
    $self->{delimit} = " ";

    return $self;
}

sub Build {

    my $self = shift;
    my $parameters = { @_ };
    $self->{result} = [ ];

    return unless ($parameters->{content});
    return unless ($parameters->{max_length});
    my @rr = split ($self->{split}, $parameters->{content});
    my @r;
    foreach (@rr) {

	my $in = '';
	$in = Trim ($_);
	if ($in) {

	    push @r, $in;
	}
    }

    return unless ( scalar (@r) );
    @rr = sort @r;

    foreach (@rr) {

	my $in = $_;
	my $size_res = scalar ( @{$self->{result}} );
	unless ($size_res) {

	    push @{$self->{result}}, $in;

	} else {

	    my $in_size = length $in;
	    my $last = $self->{result}->[-1];
	    my $last_size = length $last;
	    my $all = $last.$self->{delimit}.$in;
	    my $all_size = length $all;

	    if ($all_size > $parameters->{max_length}) {

		push @{$self->{result}}, $in;

	    } else {

		$self->{result}->[-1] = $all;
	    }
	}
    }
    return;
}

sub Get {

    my $self = shift;
    return $self->{result};
}

1;
__END__
# Below is stub documentation for your module. You'd better edit it!
