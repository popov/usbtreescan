# Copyright (c) 2014, Boris Popov <popov.b@gmail.com>
# 
# Permission to use, copy, modify, and/or distribute
# this software for any purpose with or without fee
# is hereby granted, provided that the above copyright
# notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
# ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
# DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
# THE USE OR PERFORMANCE OF THIS SOFTWARE.

package UsbTreeScan::Txt;

use 5.014002;
use strict;
use warnings;
use UsbTreeScan::BlockBuilder;

sub new {

    my $class = shift;
    my $self = { @_ };
    bless ($self, $class);
    $self->{result} = "";
    $self->{levels} = [ ];
    $self->{bb} = UsbTreeScan::BlockBuilder->new;
    return $self;
}

sub Renew {

    my $self = shift;
    $self->{result} = "";
    $self->{levels} = [ ];
    return;
}

sub Set {

    my $self = shift;
    my $in = { @_ };

    my $name     = $in->{name};
    my $level    = $in->{level};
    my $last     = $in->{last_node};
    my $content  = $in->{content};
    my $children = $in->{children};

    return unless defined $name;
    return unless defined $level;

    $self->AddLevel ($level);
    if (defined $last) {

	if ($last) {

	    $self->RemoveLevel ($level - 1);
	}
    }

    my $res = $self->{bb}->Build (
	name      => $name,
	level     => $level,
	content   => $content,
	levels    => $self->{levels},
	children  => $children
	);

    if ($res) {

	$self->{result} .= $res;
    }

    return;
}

sub AddLevel {

    my $self = shift;
    my $level = shift;

    foreach ( @{$self->{levels}} ) {

	if ($_ == $level) {

	    return;
	}
    }

    push @{$self->{levels}}, $level;
    @{$self->{levels}} = sort @{$self->{levels}};

    return;
}

sub RemoveLevel {

    my $self = shift;
    my $rmlv = shift;

    my @newl;
    foreach ( @{$self->{levels}} ) {

	next if ($_ == $rmlv);
	push @newl, $_;
    }

    @{$self->{levels}} = @newl;
    return;
}

sub Get {

    my $self = shift;
    return $self->{result};
}

1;
__END__
# Below is stub documentation for your module. You'd better edit it!
